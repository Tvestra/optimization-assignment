﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    public GameObject m_currentCollectible;

    public GameObject[] spawnPoints;

    // Update is called once per frame
    void Awake()
    {
        if (!m_currentCollectible)
        {
            m_currentCollectible =  Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.childCount ) ).position, Collectible.transform.rotation );
            m_currentCollectible.transform.parent = gameObject.transform;

        }
    }
    public void RespawnCollectable()
    {
        int rand = Random.Range(0, spawnPoints.Length);
        Vector3 newLoc = spawnPoints[rand].transform.position;
        Debug.Log(newLoc);
        m_currentCollectible.transform.position = newLoc;
    }
}
