﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        NavTarget();
	}

    void NavTarget()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Camera camera = Camera.main.GetComponent<Camera>();
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                GetComponent<NavMeshAgent>().destination = hit.point;
            }
        }
    }
}
