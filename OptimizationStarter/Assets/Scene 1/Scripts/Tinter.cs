﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
    public Material[] materials;
    Camera cam;
    // Use this for initialization
    void Awake()
    {
        cam = Camera.main;
        //Issue01: The game creates a material each tim eth eobject is spawned. [SOLVED]
        //GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );
        if (!this.gameObject.GetComponent<Camera>())
        {
            this.GetComponent<Renderer>().material.color = cam.GetComponent<Tinter>().Tint().color;

        }
    }

    public Material Tint()
    {
        //generate a random number from 0 - however long materials is,
        int randomMat = Random.Range(0, materials.Length);
        //Return the material at that number in the array
        Debug.Log((randomMat + 1) + "/" + (materials.Length));
        return materials[randomMat];
    }

}
