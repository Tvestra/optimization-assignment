﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;
    public Tinter tint;
    int lifeTime = 4;

    // Update is called once per frame
    void Update()
    {
        //When whe left mouse is held down,
        if ( Input.GetButton( "Fire1" ) )
        {
            //create a vector3 that is equal to the mouse's position relative to the screen space and faces forward.
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            //Establish a firing direction that is equal to where the mouse is and facinf forwards.
            Vector3 FireDirection = clickPoint - this.transform.position;
            //Normalize the vector
            FireDirection.Normalize();
            //Instantiate clones of Prefab under prefabInstance
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            //get the rendering component of the clone and make its material equal to the one returned by the Tinter script.
            prefabInstance.GetComponent<Renderer>().material = tint.Tint();
            //Shoot the ball forward at a rate of Firespeed
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
            //After the specified lifetime, destroy the prefab. [SOLVED]
            Destroy(prefabInstance, lifeTime);
        }
    }

}
